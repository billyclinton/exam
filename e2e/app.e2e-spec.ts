import { RegistrationWebsitePage } from './app.po';

describe('registration-website App', function() {
  let page: RegistrationWebsitePage;

  beforeEach(() => {
    page = new RegistrationWebsitePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
