import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

 constructor(private UserList:ApiService) { }

  data:Object[];
  
  ngOnInit() {
    this.UserList.getData().subscribe(result=> this.data = result);
  }
  addUser(){
    this.UserList.addData( { "name":"name1" } );
  }
}
