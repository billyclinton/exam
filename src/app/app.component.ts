import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  userData:Object[] = [
    {"ID":1, "Name":"Sam", "Address":"NY", "Email":"sam@abc.com"},
    {"ID":2, "Name":"Tomy", "Address":"ALBAMA", "Email":"tomy@abc.com"},
    {"ID":3, "Name":"Kelly", "Address":"NEBRASKA", "Email":"kelly@abc.com"}
  ]
}