import { Component, OnInit } from '@angular/core';
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-api',
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.css']
})
export class ApiComponent implements OnInit {

  constructor(private UserList:ApiService) { }

  data:Object[];
  
  ngOnInit() {
    this.UserList.getData().subscribe(result=> this.data = result);
  }
  addUser(){
    this.UserList.addData( { "name":"name1" } );
  }
}
