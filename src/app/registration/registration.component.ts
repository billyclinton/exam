import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  // @Input('uData') user_data:Object[];

  constructor(private UserList:ApiService) { }

  data:Object[];

  ngOnInit() {
  }

  username:string ="";
  nameID:string = "";
  emailID:string = "";
  addressID:string="";

  addToList(){
    var id;
    if (this.data.length == 0) {
      id=1;
    }else{
      id = this.data[this.data.length-1]["ID"]+1;
    }
    
    this.data.push({"ID":id, "Name":this.nameID, "Username":this.username,"Email":this.emailID, "Address":this.addressID});
  }

  resetForm(){
    this.nameID = "";
    this.emailID = "";
    this.addressID = "";
    this.username = "";
  }

}

