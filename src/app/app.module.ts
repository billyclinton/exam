import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';


import { ApiService } from './service/api.service';

import { AppComponent } from './app.component';
import { ListComponent } from './list/list.component';
import { RegistrationComponent } from './registration/registration.component';
import { ApiComponent } from './api/api.component';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    RegistrationComponent,
    ApiComponent
  ],
  imports: [
    BrowserModule,FormsModule,RouterModule.forRoot([
      		{path: '', component: ListComponent},
      		{path: 'user_registration', component: RegistrationComponent}
    		])
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }